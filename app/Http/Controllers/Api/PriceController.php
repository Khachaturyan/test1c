<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Price;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class PriceController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $price = Price::all();
        return $this->getSuccessResponse('done', $price);
    }

    /**
     * @param Request $request
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $product_id)
    {

        $validation_array = array_merge($request->all(),['product_id'=>$product_id]);

        $validator = Validator::make($validation_array, [
            'prices' => 'array|required',
            'prices.*.price'=>'required|numeric|between:1,500000.00',
            'prices.*.id'=>'required|uuid',
            'product_id'=>'required|uuid|exists:products,id'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $product = Product::find($product_id);
        $data = $request->all();
        $final_array = [];
        foreach ($data as $key => $prices) {
            foreach ($prices as $price){
                $final_array[] = array_merge($price,['product_id'=>$product->id]);
            }
        }
        $time = now();
        $price = new Price();
        $price->upsert($final_array,['id'],['price']);

        Price::where('product_id',$product_id)->where('updated_at','<',$time)->delete();

       return $this->getSuccessResponse('done');
    }
}
