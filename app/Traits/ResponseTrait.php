<?php

namespace App\Traits;


trait ResponseTrait
{
    /**
     * @param mixed string
     * @param array $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSuccessResponse($message, $data = [], $status = 200)
    {

        $return = [
            'status' => 1,
            'message' => $message,
            'data' => $data
        ];

        return response()->json($return, $status);
    }

    /**
     * @param $message
     * @param array $errors
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFailResponse($message, $errors = [], $status = 400)
    {
        return response()->json([
            'status' => 0,
            'message' => $message,
            'errors' => $errors,
            'data' => []
        ], $status);
    }
}
