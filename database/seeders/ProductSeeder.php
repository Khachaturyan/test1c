<?php

namespace Database\Seeders;

use App\Models\Price;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::factory()
            ->count(10000)
            ->create()
            ->each(function ($product) {
                Price::factory()->count(100)->create([
                    'product_id' => $product->id
                ]);
            });
    }
}
