#Destination calculate service

##Installation
to install ... 

clone repository:

```shell 
$ git clone [url]
```

install the dependencies: 

```shell 
$ composer install
```

for migration run command
```shell
$ php artisan migrate
```

for seeding data run command
```shell
$ php artisan db:seed
```


